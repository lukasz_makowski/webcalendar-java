package com.makowski.calendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebcalandarApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebcalandarApplication.class, args);
	}
}
